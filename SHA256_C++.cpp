#include "crypto++/cryptlib.h"
#include "crypto++/sha.h"
#include "crypto++/hex.h"
#include <iostream>
using namespace std;

string SHA256(string);

int main (int argc, char* argv[]) {
   CryptoPP::SHA256 hash;
    byte digest[ CryptoPP::SHA256::DIGESTSIZE ];
     string message = argv[1];//"abc";

    hash.CalculateDigest( digest, reinterpret_cast<byte*>(&message[0]), message.length() );
 
    CryptoPP::HexEncoder encoder;
     std::string output;
    encoder.Attach( new CryptoPP::StringSink( output ) );
        encoder.Put( digest, sizeof(digest) );
    encoder.MessageEnd();

    std::cout << " " << std::endl << "SHA(abc): " << output << std::endl;
}

string SHA256(string data)
{
    byte const* pbData = (byte*) data.data();
    unsigned int nDataLen = data.size();
    byte abDigest[CryptoPP::SHA256::DIGESTSIZE];

    CryptoPP::SHA256().CalculateDigest(abDigest, pbData, nDataLen);

    return string((char*)abDigest);
}
