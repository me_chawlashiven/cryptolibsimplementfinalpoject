//package BouncyCastleImplementation;

import org.bouncycastle.crypto.StreamCipher;
import org.bouncycastle.crypto.engines.Salsa20Engine;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;

import java.io.File;
import java.io.FileInputStream;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.security.SecureRandom;

// -----------------------------------------------------------------------------

public class SALSA20_BC {
    // this is a class in name only as all its methods are static.
    //   ...yes I know, but this is only a simple example.

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("\n  Usage:");
            System.out.println(  "    javac SALSA20_BC.java ");
            System.out.println(  "    java  SALSA20_BC <file> ");
            System.out.println("\n  Bouncy Castle 1.36 or later must be installed. \n");
            return;
        }
        String filename = args[0];
        String key = "key";                    // must be 16 or 32    bytes
        if (key.length() != 16 && key.length() != 32) {
            System.out.println("\n  *** key must be 16 or 32 bytes, so it will *** ");
            System.out.println(  "  *** be padded or truncated as appropriate  *** \n");
        }
        key = padnulls(key, 16);                // for now, pad and truncate to 16

        byte [] nonce = str2byt("12345678");    // must be 8 bytes -- best if random

        // setup parameters (key and nonce)
        KeyParameter     keyparam = new KeyParameter(str2byt(key));
        ParametersWithIV params   = new ParametersWithIV(keyparam, nonce);


        byte[] content = loadfmfile(filename);
        System.out.println("\nBefore: \n" + byt2str(content));

        StreamCipher salsa = new Salsa20Engine();
        salsa.init(true, params);

        byte[]  ciphertext = new byte[content.length];
        // loop over this next line if you want to process multiple chunks
        // just be sure chunk size is a multiple of 64 bytes
        salsa.processBytes(content, 0, content.length, ciphertext, 0);

        String newfilename = filename+".s20";
        savetofile(newfilename, concat(nonce, ciphertext));


        byte[] content2 = loadfmfile(newfilename);
        nonce = extract(content2, 0, 8);
        ciphertext = extract(content2, 8, content2.length);

        // setup parameters (key and nonce)
        KeyParameter     keyparam2 = new KeyParameter(str2byt(key));
        ParametersWithIV params2   = new ParametersWithIV(keyparam2, nonce);

        StreamCipher salsa2 = new Salsa20Engine();
        salsa2.init(true, params2);

        byte[]  plaintext = new byte[ciphertext.length];
        // loop over this next line if you want to process multiple chunks
        // just be sure chunk size is a multiple of 64 bytes
        salsa2.processBytes(ciphertext, 0, ciphertext.length, plaintext, 0);

        System.out.println("\nAfter: \n" + byt2str(plaintext));


        if (Arrays.equals(content, plaintext)) {
            System.out.println("\n  *** good *** \n");
        } else {
            System.out.println("\n  *** bad *** \n");
        }
    }

    // -------------------------------------------------------------------------
    // helpful utility functions

    // read a small to medium sized file and return its contents
    private static byte[] loadfmfile(String filename) {
        int chunksize = 2000;            // <<<<<<<<<<<<<<<<<<<<<<<<
        int len = 0;
        byte[] buf = new byte[chunksize];

        File file = new File(filename);
        FileInputStream     fis = null;
        BufferedInputStream bis = null;
        DataInputStream     dis = null;

        try {
          fis = new FileInputStream(file);
          bis = new BufferedInputStream(fis);
          dis = new DataInputStream(bis);

//          while (dis.available() != 0) {
            len = dis.read(buf, 0, chunksize);
//          }

          dis.close();
          bis.close();
          fis.close();
        } catch (FileNotFoundException e) {
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        }

        return extract(buf, 0, len);
    }

    // write content to a file
    private static void savetofile(String newfilename, byte[] content) {
        int  len  = content.length;
        File file = new File(newfilename);
        FileOutputStream     fos = null;
        BufferedOutputStream bos = null;
        DataOutputStream     dos = null;

        try {
          fos = new FileOutputStream(file);
          bos = new BufferedOutputStream(fos);
          dos = new DataOutputStream(bos);

          dos.write(content, 0, len);

          dos.close();
          bos.close();
          fos.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
    }

    private static String nullstring(int len) {
        byte zero = 0;
        byte[] nulls = new byte[len];
        Arrays.fill(nulls, zero);
        return new String(nulls);
    }

    private static String padnulls(String str, int len) {
        return str.concat(nullstring(len)).substring(0, len);
    }

    private static String byt2str(byte[] content) {
        return new String(content);
    }

    private static byte[] str2byt(String str) {
        return str.getBytes();
    }

    private static byte[] concat(byte[] b1, byte[] b2) {
        byte[] b3 = new byte[b1.length + b2.length];
        System.arraycopy(b1, 0, b3, 0, b1.length);
        System.arraycopy(b2, 0, b3, b1.length, b2.length);
        return b3;
    }

    private static byte[] extract(byte[] b, int fm, int to) {
        byte[] b2 = new byte[to - fm];
        System.arraycopy(b, fm, b2, 0, to - fm);
        return b2;
    }

}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
