#!/bin/bash

#Install JDK and JRE:
sudo apt-get update
sudo apt-get install default-jdk
sudo apt-get install default-jre

#Install Cryptopp-Dev:
sudo apt-get update
sudo apt-get install libcrypto++-dev libcrypto++-doc libcrypto++-utils

#Measure Time:
clear
echo -e "Time for each Algorithm Implementation:\n" > time.txt
for f in *.class
do
  echo "Processing $f file..."
  echo "Processing $f file..." >> time.txt
	( time echo $(java -cp .:bcprov-jdk15on-153.jar ${f%%.*} ThisIsPlainText ) >> time.txt ) 2>>time.txt;
	echo -e "\n----\n" >> time.txt;
done

for f in $(\ls . --ignore='*.*');
do
	if test -x $f; then
		echo "Processing $f file..."
		echo "Processing $f file...">>time.txt
		#echo -e $(./"$f" "ThisIsPlainText") >> time.txt
		( time echo $(./"$f" "ThisIsPlainText") >> time.txt ) 2>>time.txt;
		echo -e "\n----\n" >> time.txt;
	fi
done

#( time echo $(java -cp .:bcprov-jdk15on-153.jar AES_BC ThisIsPlainText ) > time.txt ) 2>>time.txt; echo -e "\n----\n" >> time.txt;
