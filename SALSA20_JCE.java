import java.security.*;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class SALSA20_JCE {

    private final Cipher cipher;
    private final SecretKeySpec spec;
    private final byte[] IV_8 = "AAAAAAAA".getBytes();
    public static void main(String[] args) throws Exception {

        printAvailableServices("Cipher");
        printAvailableServices("KeyGenerator");

        String keyAlgorithm = "SALSA20";
        String transformation = "SALSA20";

        try {
            int keysize = Integer.parseInt("128");
            //System.out.println("Program instantiated with " + keyAlgorithm + " as key generating algorithm, " + transformation + " as transformation algorithm and " + keysize + " as key size.\n");

            //Scanner scanner = new Scanner(System.in);
            //System.out.println("Your message: ");
            String message = args[0];

            SALSA20_JCE d = new SALSA20_JCE(keysize, keyAlgorithm, transformation);

            byte[] encryptedMsg = d.encrypt(message);
            byte[] decryptedMsg = d.decrypt(encryptedMsg);

            System.out.println("\nencrypted message: " + convertToHex(encryptedMsg));
            System.out.println("decrypted message as Hex: " + convertToHex(decryptedMsg));
            System.out.println("decrypted message as String: " + new String(decryptedMsg));

        } catch (NoSuchAlgorithmException e) {
            System.err.println("wrong algorithm input\n" + e.getMessage());
        } catch (NoSuchPaddingException e) {
            System.err.println("wrong padding input\n" + e.getMessage());
        } catch (NumberFormatException e) {
            System.err.println("third argument for key size is not a number\n" + e.getMessage());
        } catch (InvalidParameterException | InvalidKeyException e) {
            System.err.println(e.getMessage());
        }
    }

    public SALSA20_JCE(int keysize, String algorithm, String transformation)
            throws NoSuchAlgorithmException, NoSuchPaddingException {
        KeyGenerator keygen = KeyGenerator.getInstance(algorithm);
        keygen.init(keysize);
        SecretKey skey = keygen.generateKey();

        spec = new SecretKeySpec(skey.getEncoded(), algorithm);

        cipher = Cipher.getInstance(transformation);
    }

    private byte[] decrypt(byte[] msg) throws InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
        cipher.init(Cipher.DECRYPT_MODE, spec,new IvParameterSpec(IV_8));
        return cipher.doFinal(msg);
    }

    private byte[] encrypt(String msg) throws InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {

        System.out.println(IV_8.length);
        cipher.init(Cipher.ENCRYPT_MODE, spec,new IvParameterSpec(IV_8));
        System.out.println("m here ");
        return cipher.doFinal(msg.getBytes());
    }

    public static String convertToHex(byte array[]) {
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            if ((array[i] & 0xff) < 0x10)
                buffer.append("0");
            buffer.append(Integer.toString(array[i] & 0xff, 16));
        }
        return buffer.toString();
    }

    public static Set<String> getCryptImplementations(String serviceType) {
        Set<String> result = new HashSet<String>();

        for (Provider provider : Security.getProviders()) {
            Set<Object> keys = provider.keySet();
            for (Object k : keys) {
                String key = ((String) k).split(" ")[0];

                if (key.startsWith(serviceType + ".")) {
                    result.add(key.substring(serviceType.length() + 1));
                } else if (key.startsWith("Alg.Alias." + serviceType + ".")) {
                    result.add(key.substring(serviceType.length() + 11));
                }
            }
        }
        return result;
    }

    /*public static Set<String> getServiceTypes() {
        Set<String> result = new HashSet<String>();

        for (Provider provider : Security.getProviders()) {
            Set<Object> keys = provider.keySet();
            for (Object k : keys) {
                String key = ((String) k).split(" ")[0];

                if (key.startsWith("Alg.Alias.")) {
                    key = key.substring(10);
                }
                int ix = key.indexOf('.');
                result.add(key.substring(0, ix));
            }
        }
        return result;
    }*/

    public static void printAvailableServices(String service) {
        boolean first = true;
        System.out.println("List of available " + service + "s");
        for (String s : getCryptImplementations(service)) {
            System.out.print(first ? s : ", " + s);
            first = false;
        }
        System.out.println();
        System.out.println();
    }
}
